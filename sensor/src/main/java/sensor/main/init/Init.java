package sensor.main.init;

import sensor.main.clases.Cliente;
import sensor.main.clases.LoggerServicio;
import sensor.main.clases.Servicio;

public class Init {

	public static void main(String[] args) {

		Cliente c = new Cliente (new LoggerServicio(new Servicio()));
		c.consumirServicio();
	}

}
