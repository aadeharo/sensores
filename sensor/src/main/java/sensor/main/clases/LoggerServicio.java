package sensor.main.clases;

public class LoggerServicio extends Servicio {
	private Servicio servicio;


	public LoggerServicio(Servicio servicio) {
		super();
		this.servicio = servicio;
	}
	
	public void metodoConsumir(int x) {
		System.out.println("consume el metodo con el valor "+x);
		servicio.metodoConsumir(x);
	}
}
